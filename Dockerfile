FROM rocker/tidyverse:3.6.0

RUN set -e \
  && apt-get update -qq \
  && apt-get -y --no-install-recommends install \
    libmagick++-dev \
  && rm -r /var/lib/apt/lists/* \
  && install2.r --error \
    dplR
